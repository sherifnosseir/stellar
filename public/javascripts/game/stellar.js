//TODO
//request upgrades

$(document).ready(function() {
count = 0;

function updateStatus() {
  $("#status #phaseLabel").html(phase);
  $("#status #statusLabel").html(status);
  if (player.details.username != undefined) {
    $("#status #usernameLabel").html(player.details.username + "-" + player.client_id + "(" + player.details.title + ")");
  }
}

function goToStep(step) {
  $("#orbit-menu").hide();
  if (step == 1) {
    $('#step-1').show();
    $('#step-2').hide();
    $('#step-3').hide();

    $("#start-game-button").hide();
    $("#ext-step-button").show();
  } else if (step == 2) {
    $('#step-1').hide();
    $('#step-2').show();
    $('#step-3').hide();

    $("#start-game-button").hide();
    $("#next-step-button").show();
  } else if (step == 3) {
    $('#step-1').hide();
    $('#step-2').hide();
    $('#step-3').show();

    $("#start-game-button").show();
    $("#next-step-button").hide();
  }
}

function goToPhase(phase) {
//pre game
  if (phase == 0) {
    $('#gameView').hide();
    $('#preGameDiv').show(); 
  } else if (phase == 1) { // playing game
    $('#gameView').show();
    $('#preGameDiv').hide();
  }
}

if($("#loggedUser").length) {
  playerInfo.username = $("#loggedUser").data("user");
} else {
  playerInfo.username = "Anonymous";
}

var socket = io('http://localhost:3000', { query: 'user=' + playerInfo.username });
// var socket = io('http://192.168.1.67:3000', {query: 'user=' + player.username});


//socket used to broadcast live games on tv page
var tvSocket = io('http://localhost:3000/tv');

// socket used to broadcast events to monitoring page
var monitorSocket = io('http://localhost:3000/monitor');


//start broadcasting game while the player gets profile set up

/*
* When the game page is loaded, fire a join event to join the game room
*/
socket.emit('join', {
  'race': 'unknown'
});

socket.on('wait', function (basicPlayer) {

  playerInfo = basicPlayer;

  // draw();
  phase = "pre-game";
  status = "selecting-username"; // waiting to start

  goToPhase(0);
  goToStep(1);
});

function goToNextStep() {
  if (status == "selecting-username") {
    playerInfo.username = $('#usernameField').val();

    if (playerInfo.username != "" && playerInfo.username != undefined) {
      //prepare next step
      status = "selecting-race"
      goToStep(2)
    }
  } else if (status == "selecting-race") {
    // player.race = $
    //prepare next step
    if (playerInfo.race != undefined) {
      status = "selecting-ship"
      goToStep(3)
    }
  } else if (status == "selecting-ship") {
      if (playerInfo.ship != undefined) {
      status = "EOPGP"; // end of pre game phase
      phase = "playing"

      socket.emit('ready', playerInfo);
    }
  }
}

function goToPreviousStep() {
  if (status == "selecting-username") {
    status = "selecting-username";

    // goToStep(2) 
    goToStep(1);
  } else if (status == "selecting-race") {
    // player.race = $
    //prepare next step
    status = "selecting-username";
    goToStep(1);
  } else if (status == "selecting-ship") {

    //prepare next step
    status = "selecting-race"; // end of pre game phase
    goToStep(2);
  }
}


// In game upgrades and orbitting menus
function closeMenu() {
  menuStatus = -1;

  $("#upgrades-menu").hide();
  $("#orbit-menu").hide();
  $("#canvas").show();
}

function showMenu(menuNumber) {
  menuStatus = menuNumber;

  $("#canvas").hide();
  switch (menuStatus) {
    case 0: // orbit menu
      $("#upgrades-menu").hide();
      $("#orbit-menu").show();
    break;
    case 1: // weapon upgrades menu
      $("#orbit-menu").hide();
      $("#upgrades-menu").show();
    break;
  }
}


// Orbitting stuff
$("#orbit-menu").on("click", ".orbit-tile", function(){
  var selectedPlanet = $(this).attr('id');

  if (selectedPlanet == '-1') {
    socket.emit("go-to-closedst-orbit");
  } else {
    // console.log("going to orbit " + selectedPlanet);
    socket.emit("go-to-orbit", selectedPlanet);
  }
  closeMenu();
});

socket.on("display-orbit-planets", function(planets) {
  $("#orbit-menu").html("");
  $("#orbit-menu").append("<div class='orbit-tile' id='-1'><span class='glyphicon glyphicon-repeat'></span><p>Nearest Orbit</p></div>");
  // nearest colony option

  for (var key in planets) {
    var planet = planets[key];
    if (planet.race == player.race) {
      // $("#orbit-menu").append("<p>" + planet.name +"</p>");
      var planetDiv = "<div class='orbit-tile' id='"+planet.name+"'><img class='img-thumbnail' src='"+planet.src+"'><p>" + planet.name + "</p></div>";
      $("#orbit-menu").append(planetDiv);
    }
  }
  showMenu(0);
});


// Shopping stuff
socket.on("show-upgrade-equipment", function(data) {
  if(! $(".upgrade-tile").length) {
    $("#upgrades-menu").html("");

    for (var key in data) {
      var equipment = data[key];

      $("#upgrades-menu").append("<div class='upgrade-section'>"); // starting of upgrade section
      var insertedHTML = ""
      insertedHTML += "<div>" + key + "</div>" + "<div class='upgrade-tile row'>";

      for (var i = 0; i < equipment.upgrades.length; i++) {
        insertedHTML += "<div class='upgrade-slot col-md-2' type='" + key + "' level='" + equipment.upgrades[i].id + "'>" + equipment.name + " " + equipment.upgrades[i].id + "</div>";
      }

      insertedHTML += "</div></div>";
      $("#upgrades-menu").append(insertedHTML); // end of upgrade section
    }
  }
  showMenu(1);
});

// Orbitting stuff
$("#upgrades-menu").on("click", ".upgrade-tile .upgrade-slot", function(){
  // console.log($(this).attr("type") + ": " + $(this).attr("level") + " pressed");
  var upgradeType = $(this).attr("type");
  var upgradeLevel = $(this).attr("level")
  socket.emit("request-upgrade", upgradeType, upgradeLevel);
});


// Pre Game Menu Navigation
$('#back-step-button').click(function() {
  goToPreviousStep();
});

$('#next-step-button').click(function(){
  goToNextStep();
});

$('#start-game-button').click(function(){
  if (playerInfo.ship != undefined) {
    status = "EOPGP"; // end of pre game phase
    phase = "playing"

    socket.emit('ready', player);
  }
});



/*
* Player's info is sent to server to get them set up, info about others will come as updates
*/
socket.on('ready', function (playerData, shipData, worldData) {
  //player and ship info
  player = playerData;
  ship = shipData;
  
  goToPhase(1);
  status = "start-game";
});

//, shipData, worldData
socket.on('update', function(playerData, shipsData, worldData) {
  updateStatus();

  if (phase == "playing") {

    player = playerData[player.client_id];
    player.ship = shipsData[player.client_id];

    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0 , 0, canvas.width, canvas.height);

    // ctx.fillStyle = "rgb(0,0,0)";
    // ctx.fillRect(0, 0, canvas.width, canvas.height);

    drawWorld(worldData);
    draw(playerData, shipsData);
    // drawStarbase(worldData.starbases);
    drawSideBar(shipsData);

    if (status == "start-game") {
    //set up player on client side for the first update loop then start playing
    // orbit home planet
    status = "roaming";
  } else if (status == "docking") { // docking - docked
    
  } else if (status == "orbit") { // orbitting - in-orbit

  } else if (status == "roaming") {
    
  }

  if (player.status.state != undefined) {
    if (player.status.state == "docked") {
      socket.emit("request-upgrade-equipment");
    } 
  }
}
});

/*
* Show error message on login failure
*/
if ($("#loginError").length && !$("#loginError").is(':empty')) {

  Messenger({
    extraClasses: 'messenger-fixed messenger-on-right messenger-on-top'
  }).post({
    message: $("#loginError").html(),
    type: 'error',
    showCloseButton: true,
    hideAfter: 10
  });
}

/*
* Show error message on registration failure
*/
if ($("#registerError").length && !$("#registerError").is(':empty')) {

  Messenger({
    extraClasses: 'messenger-fixed messenger-on-right messenger-on-top'
  }).post({
    message: $("#registerError").html(),
    type: 'error',
    showCloseButton: true,
    hideAfter: 10
  });
}

/*
* Show message on successful logout
*/
if ($("#logoutSuccess").length && !$("#logoutSuccess").is(':empty')) {

  Messenger({
    extraClasses: 'messenger-fixed messenger-on-right messenger-on-top'
  }).post({
    message: $("#logoutSuccess").html(),
    type: 'success',
    showCloseButton: true,
    hideAfter: 10
  });
}

/*
* Show welcome message on registration success
*/
if ($("#registerSuccess").length && !$("#registerSuccess").is(':empty')) {

  Messenger({
    extraClasses: 'messenger-fixed messenger-on-right messenger-on-top'
  }).post({
    message: $("#registerSuccess").html(),
    type: 'success',
    showCloseButton: true,
    hideAfter: 10
  });
}

/*
* Show welcome message on login success
*/
if ($("#welcomeMessage").length && !$("#welcomeMessage").is(':empty')) {

  Messenger({
    extraClasses: 'messenger-fixed messenger-on-right messenger-on-top'
  }).post({
    message: $("#welcomeMessage").html(),
    type: 'success',
    showCloseButton: true,
    hideAfter: 10
  });
}

//emit the button that is pressed
$(document).keydown(function(e) {
  if (e.which === 8 && !$(e.target).is("input, textarea")) {
        e.preventDefault();
   }

  if (phase == "playing") {
    switch(e.which) {
        case 37: // left
        socket.emit("start-rotate-player-left-update");
        break;

        case 38: // up
        socket.emit("start-accelerate-player-update");
        break;
        
        case 39: // right
        socket.emit("start-rotate-player-right-update");
        break;
        
        case 40: // down        
        socket.emit("start-deccelerate-player-update");
        break;

        case 32: // space
        socket.emit("player-shoot-update");
        break;

        case 13: // Enter
        break;

        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  }
});

//emit something when button is released
$(document).keyup(function(e) {
  if (phase == "playing") {
    if (e.which >= 49 && e.which <= 54) {
      socket.emit("change-selected-weapon", e.which - 49);
    } else {
      switch(e.which) {
          case 37: // left
          socket.emit("stop-rotate-player-left-update");
          break;

          case 38: // up
          socket.emit("stop-accelerate-player-update");
          break;
          
          case 39: // right
          socket.emit("stop-rotate-player-right-update");
          break;
          
          case 40: // down
          socket.emit("stop-deccelerate-player-update");
          break;

          case 32: // space
          break;

          case 13: // Enter
          break;

          case 79:// o -> orbit
          // show player orbit menu
          if (menuStatus != 0) {
            socket.emit('request-orbit-planets');
          }
          break;

          case 68:// d -> docking
          socket.emit("go-to-dock");
          closeMenu();
          break;

          case 74: // j -> request colony
          if (player.status.state == "in-orbit") {
            socket.emit("request-colony");
          }
          break;

          case 191: // / -> drop colony && get out of docking
          if (menuStatus == 1) {
            socket.emit("done-upgrading");
            closeMenu();
          } else {
            if (player.attachment != undefined) {
              if (player.attachment.type == "colony") {
                socket.emit("drop-attachment");
              }
            }
          }
          break;

          default: return; // exit this handler for other keys
      }
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  } else if (phase == "pre-game") {
       switch(e.which) {
        case 13: // Enter
          goToNextStep();
        break;
        default: return; 
        }
  //     if (status == "selecting-username") {
  //       switch(e.which) {
  //         case 13: // Enter
  //         goToNextStep();
  //         break;
  //         case 8: // backspace
  //         default: return; 
  //       }
  // } else if (status == "selecting-race") {
  //     switch(e.which) {
  //       case 13: // Enter
  //       goToNextStep();
  //       break;
  //       default: return; 
  //     }
  // } else if (status == "selecting-ship") {
  //     switch(e.which) {
  //       case 13: // Enter
  //       goToNextStep();
  //       break;
  //       default: return; 
  //     }
  //   }
  }
});

});