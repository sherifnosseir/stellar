scoutShipSprite = new Image();
scoutShipSprite.src = "images/scout.png";

// rects = []

// testRect = new Object();
// testRect.x = 50;
// testRect.y = 250;


// // starbase
// testRect1 = new Object();
// testRect1.x = 200;
// testRect1.y = 250;
// testRect1.r = 50;

// //planet
// testRect2 = new Object();
// testRect2.x = 400;
// // testRect2.x = 450;
// testRect2.y = 250;
// testRect2.r = 200;

// testRect3 = new Object();
// testRect3.x = 150;
// testRect3.y = 50;

// testRect4 = new Object();
// testRect4.x = 350;
// testRect4.y = 50;

// // rects.push(testRect)
// rects.push(testRect1)
// rects.push(testRect2)
// // rects.push(testRect3)
// // rects.push(testRect4)

var view = new Object();

function getRaceColor(race) {
  if (race == "Terran") {
    return "rgb(0,0,225)";
  } else if (race == "Drengin") {
    return "rgb(225,0,0)";
  } else if (race == "Arcanine") {
    return "rgb(225,225,0)";
  } else if (race == "Pirate") {
    return "rgb(0,0,225)";
  } else {
      //grey
      return "rgb(192,192,192)";
  }
}

function getColonyImage(race) {
  if (race == "Terran") {
    return "images/colonies/terran-colony.png";
  } else if (race == "Drengin") {
    return "images/colonies/drengin-colony.png";
  } else if (race == "Arcanine") {
    return "rgb(225,225,0)";
  } else if (race == "Pirate") {
      return "rgb(0,0,225)";
  } else {
      //grey
      return "rgb(192,192,192)";
  }
}

//check which tab is selected, and which index of planets or ships is chosen
//for now it only updates hp and weapon name (could show power bar next to it)
function drawSideBar(ships) {
    // $("#player-status-view #playerHP").html(ships[player.client_id].specs.hp);

    var playerShip = ships[player.client_id];
    $("#player-status-view").html("");

    $("#player-status-view").append("<p>HP: " + playerShip.specs.hp + "</p>");

    for (var key in playerShip.setup) {
      $("#player-status-view").append("<p>" + key + ": " + playerShip.setup[key].level + " -> " + playerShip.setup[key].current + "</p>");
    }
}

function drawWorld(worldData) {
  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");

  var playerX = player.ship.physx.pos.x;
  var playerY = player.ship.physx.pos.y;

   //field of view
  view.cameraX = playerX - canvas.width/2; // view should be extended a litte bit more for planet edge cases or another calculation can be used
  view.cameraY = playerY - canvas.height/2; // - ship.width/2 (height) take into account ships dimensions
  view.centerX = canvas.width/2;
  view.centerY = canvas.height/2;

  for (var key in worldData.stars) {
    var star = worldData.stars[key];


    ctx.fillStyle = "rgb(225,128,0)";
    ctx.fillRect(view.centerX + (star.x - playerX) - 25, view.centerY + (star.y - playerY) - 25, 50, 50);

    ctx.beginPath();
    ctx.arc(view.centerX + (star.x - playerX), view.centerY + (star.y - playerY), star.r, 0, Math.PI*2);
    ctx.stroke();
  }

  for (var key in worldData.planets) {
    var planet = worldData.planets[key];
  
    var planetSprite = new Image();
    planetSprite.src = planet.src;

    var colonySprite = new Image();
    colonySprite.src = getColonyImage(planet.race); 

    ctx.save();

    ctx.translate(view.centerX + (planet.x - playerX), view.centerY + (planet.y -playerY));

    ctx.drawImage(planetSprite, -planetSprite.width/2, -planetSprite.height/2);
    ctx.drawImage(colonySprite, -colonySprite.width/2, -colonySprite.height/2);

    ctx.restore();  
  }

  for (var key in worldData.moons) {
    var moon = worldData.moons[key];
    var playerX = player.ship.physx.pos.x;
    var playerY = player.ship.physx.pos.y;
    
    ctx.fillStyle = getRaceColor(moon.race);
    ctx.fillRect(view.centerX + (moon.x - playerX), view.centerY + (moon.y - playerY), 10, 10);

    ctx.beginPath();
    ctx.arc(view.centerX + (moon.x - playerX), view.centerY + (moon.y - playerY), moon.orbitRadius, 0, Math.PI*2);
    ctx.stroke();
  }

  for (var key in worldData.colonies) {
    var colony = worldData.colonies[key];
    var playerX = player.ship.physx.pos.x;
    var playerY = player.ship.physx.pos.y;

    var colonySprite = new Image();
    colonySprite.src = getColonyImage(colony.specs.race); 

    ctx.save();

    ctx.translate(view.centerX + (colony.pos.x - playerX), view.centerY + (colony.pos.y - playerY));
    ctx.drawImage(colonySprite, -colonySprite.width/2, -colonySprite.height/2);

    ctx.restore();
  }
}

function draw(players, ships) {
  // draw planets
  // draw colonies
  // draw bullets
  // draw ships
  // draw explosions

  // testRect2.x += 0.1;
  // testRect2.y += 0.1;
  
	var canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");

  var playerX = player.ship.physx.pos.x;
  var playerY = player.ship.physx.pos.y;

   //field of view
  view.cameraX = playerX - canvas.width/2;	// view should be extended a litte bit more for planet edge cases or another calculation can be used
  view.cameraY = playerY - canvas.height/2; // - ship.width/2 (height) take into account ships dimensions
  view.centerX = canvas.width/2;
  view.centerY = canvas.height/2;

  //player view is static (player is always in the middle of the view)
  $.each (players, function (client_id, user) {
    var userShip = ships[client_id];

  	if (user.client_id == player.client_id) {
      //Draw User Ship
      ctx.save();

      ctx.translate(view.centerX, view.centerY);
      ctx.rotate(userShip.physx.velocity.angle-Math.PI/2%(2*Math.PI));
      ctx.drawImage(scoutShipSprite,-10, -13);

      ctx.restore();

      // health box around player
  		ctx.rect(view.centerX-(10*1.5), view.centerY-(13*1.5), 20*1.5, 27*1.5);
  		count++;
  		ctx.stroke();
  	} else {
      //Draw opponent Ship

      ctx.save();

      ctx.translate(view.centerX + (userShip.physx.pos.x - playerX), view.centerY + (userShip.physx.pos.y - playerY));
      ctx.rotate(userShip.physx.velocity.angle-Math.PI/2%(2*Math.PI));
      ctx.drawImage(scoutShipSprite,-10, -13);

      ctx.restore();


      //maybe only around allies

      // health box around player
      // ctx.rect(view.centerX + (user.pos.x - player.pos.x) - 10 *1.5, view.centerY + (user.pos.y - player.pos.y) - 13*1.5, 20*1.5, 27*1.5);
      // count++;
      // ctx.stroke();


  		// ctx.fillStyle = "rgb(225,0,0)";
  		// ctx.fillRect(view.centerX-25 + (user.pos.x - player.pos.x), view.centerY-25 + (user.pos.y - player.pos.y), 50, 50);
  	}

    if (user.attachment != undefined) {
      if (user.attachment.type == "colony") {
        var offsetX = 25*Math.cos(userShip.physx.velocity.angle);
        var offsetY = 25*Math.sin(userShip.physx.velocity.angle);


        var colonySprite = new Image();
        colonySprite.src = getColonyImage(user.race); 

        ctx.save();

        ctx.translate(view.centerX + (userShip.physx.pos.x + offsetX) - playerX, view.centerY + (userShip.physx.pos.y + offsetY) - playerY);
        ctx.drawImage(colonySprite, -colonySprite.width/2, -colonySprite.height/2);

        ctx.restore();
      }
    }

     //process player bullets
    for (var i = 0; i < user.shots.length; i++) {
        var bullet = user.shots[i];

        ctx.fillStyle = "rgb(225,0,0)";
        ctx.fillRect(view.centerX + (bullet.pos.x - playerX), view.centerY + (bullet.pos.y - playerY), 5, 5);
    }
  });

  // console.log("player : " + player.x + "," + player.y);
  //animate the rest of the world around the player
  // for(var i = 0; i < rects.length; i++) {
  // 	// if ((player.x - rects[i].x <= canvas.width/2) && (player.y - rects[i].y <= canvas.height/2)) { // check other half
  // 		// console.log("rect " + i + ": " + rects[i].x + "," + rects[i].y);
  // 		ctx.fillStyle = "rgb(0,0,225)";
  // 		ctx.fillRect(view.centerX + (rects[i].x - player.pos.x), view.centerY + (rects[i].y - player.pos.y), 10, 10);

  //     ctx.beginPath();
  //     ctx.arc(view.centerX + (rects[i].x - player.pos.x), view.centerY + (rects[i].y - player.pos.y),rects[i].r,0,Math.PI*2);
  //     ctx.stroke();
  // // }
  // }

  // ctx.drawImage(scoutShipSprite,0,0,63,81,player.x-16,player.y-20,32,41);
  // ctx.translate(-player.x, -player.y);
  // ctx.restore();
  // ctx.save();
}

// tankSprite=new Image();
// tankSprite.src = "images/tankssheet.png";
// userTankSprite = new Image();
// userTankSprite.src = "images/usertankssheet.png";
// turret=new Image();
// turret.src = "images/tankturret.png";
// userturret=new Image();
// userturret.src = "images/usertankturret.png";
// //background
// backGround = new Image();

// backGround.src = "images/backGround.png";
// smoke0 = new Image();
// smoke0.src = "images/explosionframe0.png";
// smoke1 = new Image();
// smoke1.src = "images/explosionframe1.png";
// smoke2 = new Image();
// smoke2.src = "images/explosionframe2.png";
// smoke3 = new Image();
// smoke3.src = "images/explosionframe3.png";
// smoke4 = new Image();
// smoke4.src = "images/explosionframe4.png";

// function drawTank(tank,frameNum){
	
//  	var canvas = document.getElementById("canvas");
//  	var ctx = canvas.getContext("2d");

// 	for (var i=0; i < tank.length; i++) {

// 		//Calculate Tank Angle
// 		var x = tank[i].x;
// 		var y = tank[i].y;
// 		var tankAngle = tank[i].wheelAngle;

// 		//Calculate Turret Angle
// 		var turretAngle = Math.PI;

// 		//This is a transformation to rotate objects on canvas
// 		ctx.save();
// 		ctx.translate(x+17, y+17);
// 		ctx.rotate((tankAngle+(Math.PI)/2)%(2*Math.PI));
// 		if(tank[i].id == tank_id)
// 		{
// 			//Draw User Tank
// 			ctx.drawImage(userTankSprite,0,frameNum*81,63,81,-16,-20,32,41);
// 			ctx.translate(-x, -y);
// 			ctx.restore();

// 			ctx.save();
// 			/* draw tank circle
// 			for (var m=0; m < 42; m++) {
// 			for (var n=0; n < 31; n++) {
// 				if((Math.pow(m-21, 2) + Math.pow(n-15, 2)) < Math.pow(15, 2))
// 				{


// 						ctx.fillStyle = "rgb(0,0,255)";
// 						ctx.fillRect (x+m, y+n,1,1);

// 				}
// 			}
// 			}*/


// 			var xDirection = mouseX - x;
// 			var yDirection = mouseY - y;
// 			turretAngle = Math.atan2(yDirection, xDirection);

// 			//Draw User Turret
// 			ctx.translate(x+17, y+17);
// 			ctx.rotate((turretAngle+(Math.PI)/2)%(2*Math.PI));
// 			ctx.drawImage(userturret,0,0,67,67,-17,-17,34,34);
// 		}
// 		else
// 		{
// 			//Draw Tank
// 			ctx.drawImage(tankSprite,0,frameNum*81,63,81,-16,-20,32,41);
// 			ctx.translate(-x, -y);
// 			ctx.restore();

// 			ctx.save();

// 			turretAngle = tank[i].turretAngle;

// 			//Draw Turret
// 			ctx.translate(x+17, y+17);
// 			ctx.rotate((turretAngle+(Math.PI)/2)%(2*Math.PI));

// 			ctx.drawImage(turret,0,0,67,67,-17,-17,34,34);
// 		}
// 		ctx.translate(-x, -y);
// 		ctx.restore();


// 		if(tank[i].status == "dead")
// 		{
// 			frameNum = frameNum%4;

// 			switch(frameNum)
// 			{
// 				case 1:
// 					ctx.drawImage(smoke1,0,0,50,150,x-17,y-133,50,150);
// 					break;
// 				case 2:
// 					ctx.drawImage(smoke2,0,0,50,150,x-17,y-133,50,150);
// 					break;
// 				case 3:
// 					ctx.drawImage(smoke3,0,0,50,150,x-17,y-133,50,150);
// 					break;
// 				default:
// 					ctx.drawImage(smoke0,0,0,50,150,x-17,y-133,50,150);
// 					break;

// 			}
// 		}
// 		else
// 		{
// 			//Draw Health
// 			ctx.beginPath();
// 			ctx.rect(tank[i].x-8, tank[i].y-25, tank[i].hp/2, 15);
// 			if(tank[i].hp > 75)
// 			{
// 				ctx.fillStyle = 'green';
// 			}
// 			else
// 			{
// 				if(tank[i].hp > 30)
// 				{
// 					ctx.fillStyle = 'yellow';
// 				}
// 				else
// 				{
// 					ctx.fillStyle = 'red';
// 				}
// 			}	

// 			//Draw HP
// 			ctx.fill();
// 			ctx.lineWidth = 2;
// 			ctx.strokeStyle = 'black';
// 	   		ctx.stroke();

// 			ctx.fillStyle = "black";
// 	      	ctx.font = "8pt sans-serif";
// 			ctx.textAlign = 'left';
// 		    ctx.fillText(tank[i].hp+"%", tank[i].x+5, tank[i].y-13);
// 		}

// 		//draw bullet
// 		var bullets=tank[i].weapon.bullets;
// 		for(var j = 0; j<bullets.length; j++)
// 		{
// 			//if(tank[i].id == tank_id)
// 			//{
// 				ctx.fillStyle = "rgb(0,255,255)";
// 			//}
// 			//else
// 			//{
// 			//	ctx.fillStyle = "rgb(255,0,0)";
// 			//}

// 			ctx.fillRect (bullets[j].x, bullets[j].y, 5, 5);
// 		}

// 		//Draw Username
// 		ctx.fillStyle = "black";
//       	ctx.font = "8pt sans-serif";
// 		ctx.textAlign = 'center';
// 	    ctx.fillText(tank[i].username, tank[i].x+14, tank[i].y+52);
// 	}

// }

// function drawTurret(tank){
// 	var canvas = document.getElementById("canvas");
//  	var ctx = canvas.getContext("2d");

// 	for (var i=0; i < tank.length; i++) {
// 		var x = tank[i].x;
// 		var y = tank[i].y;
// 		var angle = Math.PI;

// 		//console.log("id : " + tank_id);
// 		//console.log("Tank id : " + tank[i].id);
// 		if(tank[i].id == tank_id)
// 		{
// 			//Calculate Turret Angle
// 			var xDirection = mouseX - x;
// 			var yDirection = mouseY - y;
// 			angle = Math.atan2(yDirection, xDirection);
// 		}
// 		else
// 		{
// 			angle = tank[i].turretAngle;
// 		}


// 		//This is a transformation to rotate objects on canvas
// 		ctx.save();
// 		ctx.translate(x+17, y+17);
// 		ctx.rotate((angle+(Math.PI)/2)%(2*Math.PI));
// 		if(tank[i].id == tank_id)
// 		{
// 			ctx.drawImage(userturret,0,0,67,67,-17,-17,34,34);
// 		}
// 		else
// 		{
// 			ctx.drawImage(turret,0,0,67,67,-17,-17,34,34);
// 		}
// 		ctx.translate(-x, -y);
// 		ctx.restore();
// 	};

// }

// function drawBullets (bullets) {
// 	var canvas = document.getElementById("canvas");
//  	var ctx = canvas.getContext("2d");

// 	for(i = 0; i<bullets.length; i++)
// 	{
// 		if(bullets[i].clientID == tank_id)
// 		{
// 			ctx.fillStyle = "rgb(0,255,0)";
// 		}
// 		else
// 		{
// 			ctx.fillStyle = "rgb(255,0,0)";
// 		}

// 		ctx.fillRect (bullets[i].x, bullets[i].y, 5, 5);
// 	}
// }

// function drawCursor () {
// 	var canvas = document.getElementById("canvas");
//  	var ctx = canvas.getContext("2d");

// 	ctx.beginPath();
// 	ctx.fillStyle = "rgb(0,225,0)";
// 	ctx.moveTo(mouseX,mouseY);
// 	ctx.lineTo(mouseX+15,mouseY+5);
// 	ctx.fillStyle = "rgb(0,0,225)";
// 	ctx.lineTo(mouseX+10, mouseY+10);
// 	ctx.lineTo(mouseX+10, mouseY+20);


// 	ctx.fill();
// }

// function drawMap() {
// 	var canvas = document.getElementById("canvas");
// 	var ctx = canvas.getContext("2d");

// 	ctx.drawImage(backGround,0,0);

// 	/*
// 	for (var i=0; i < 960; i++) {
// 		for (var j=0; j < 540; j++) {
// 			if (pixelMap[i][j].type == "water") {
// 				ctx.fillStyle = "rgb(0,0,255)";
// 				ctx.fillRect (i, j, 1, 1);
// 			}
// 			else
// 			{
// 				if(pixelMap[i][j].type == "rock")
// 				{
// 					ctx.fillStyle = "rgb(139,69,19)";
// 					ctx.fillRect (i, j, 1, 1);
// 				}
// 			}
// 		};
// 	};*/
// }

// function draw(tanks, bullets, frameNum){
// 	drawMap();
// 	drawTank(tanks, frameNum);
// 	//drawBullets(bullets);
// 	drawCursor();
// }

//  function clearCanvas() {
//  	var canvas = document.getElementById("canvas");
//  	var ctx = canvas.getContext("2d");
//  	ctx.clearRect(0 , 0, 960, 540);
//  }


// ctx.drawImage(backGround,0,0);


	// for (var i=0; i < 960; i++) {
	// 	for (var j=0; j < 540; j++) {
	// 		if (pixelMap[i][j].type == "water") {
	// 			ctx.fillStyle = "rgb(0,0,255)";
	// 			ctx.fillRect (i, j, 1, 1);
	// 		}
	// 		else
	// 		{
	// 			if(pixelMap[i][j].type == "rock")
	// 			{
	// 				ctx.fillStyle = "rgb(139,69,19)";
	// 				ctx.fillRect (i, j, 1, 1);
	// 			}
	// 		}
	// 	};
	// };

