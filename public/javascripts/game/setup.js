// Race Selection

function highlightSelectedRace(race_id) {
	switch (race_id) {
		case 0:
			$("#t-race-selector").css("color", "blue");
			$("#d-race-selector").css("color", "black");
			$("#a-race-selector").css("color", "black");
			$("#p-race-selector").css("color", "black");
		break;
		case 1:
			$("#t-race-selector").css("color", "black");
			$("#d-race-selector").css("color", "red");
			$("#a-race-selector").css("color", "black");
			$("#p-race-selector").css("color", "black");
		break;
		case 2:
			$("#t-race-selector").css("color", "black");
			$("#d-race-selector").css("color", "black");
			$("#a-race-selector").css("color", "yellow");
			$("#p-race-selector").css("color", "black");
		break;
		case 3:
			$("#t-race-selector").css("color", "black");
			$("#d-race-selector").css("color", "black");
			$("#a-race-selector").css("color", "black");
			$("#p-race-selector").css("color", "green");
		break;
	}
}

$("#t-race-selector").click(function() {
	playerInfo.race = "Terran";
	highlightSelectedRace(0);
});

$("#d-race-selector").click(function() {
	playerInfo.race = "Drengin";
	highlightSelectedRace(1);
});

$("#a-race-selector").click(function() {
	playerInfo.race = "Arcanine";
	highlightSelectedRace(2);
});

$("#p-race-selector").click(function() {
	playerInfo.race = "Pirate";
	highlightSelectedRace(3);
});



// Ship Selection
function highlightSelectedShip(ship_id) {
	switch (ship_id) {
		case 0:
			$("#scout-ship-selector").css("color", "red");
			$("#another-ship-selector").css("color", "black");
		break;
		case 1:
			$("#scout-ship-selector").css("color", "black");
			$("#another-ship-selector").css("color", "red");
		break;
		case 2:

		break;
		case 3:

		break;
	}
}

$("#scout-ship-selector").click(function() {
	playerInfo.ship = "scout";
	highlightSelectedShip(0);
});

$("#another-ship-selector").click(function() {
	playerInfo.ship = "scout";
	highlightSelectedShip(1);
});