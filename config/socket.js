//TODO
//while docked:
//respond to request upgrades
//request weapon change
//
//

module.exports = function (server) {

    var io = require('socket.io').listen(server);

    // var tv = io.of('/tv'); // Socket to broadcast top rated game moves to index and tv pages

    // setInterval(function() {
    //     var possibleMoves = topRatedGame.moves();
    //     // if the game is over, reload a new game
    //     if (topRatedGame.game_over() === true || topRatedGame.in_draw() === true || possibleMoves.length === 0) {
    //         topRatedGame = new chess.Chess();
    //         possibleMoves = topRatedGame.moves();
    //     }

    //     var move = possibleMoves[Math.floor(Math.random() * possibleMoves.length)];
    //     topRatedGame.move(move);
    //     tv.emit('new-top-rated-game-move', { fen: topRatedGame.fen(), pgn: topRatedGame.pgn(), turn: topRatedGame.turn() });
    // }, 3000);

    // connect as a spectator and watch an ongoing game
    // tv.on('connection', function(socket){
    //     socket.emit('new-spectator', {"i dont know what goes here"});
    // });


    var games = {};
    var users = 0;

    var client_offset = 0;
    var userlist = new Object();
    var ships = new Object();
    
    var world = {
        stars: new Object(),
        planets: new Object(),
        moons: new Object(),
        colonies: [], // free roaming colonies
        starbases: {
            Terran: [],
            Drengin: [],
            Arcanine: [],
            Pirate: [],
        },
    };

    var equipment = {
        shields: {
            name: "Shield",
            upgrades: [{
                id: 0,
                credits: 0,
                capacity: 200,
                specs: {
                    damageReduction: 20, // percentage of damage reduced
                }
            }, {
                id: 1,
                credits: 50,
                capacity: 500,
            }]
        },
        powers: {
            name: "Power",
            upgrades: [{
                id: 0,
                credits: 0,
                capacity: 100,
            }]
        },
        engines: {
            name: "Engine",
            upgrades: [{
                id: 0,
                credits: 0,
                capacity: 100,
            }]
        },
        armors: {
            name: "Armor",
            upgrades: [{
                id: 0,
                credits: 0,
                capacity: 100,
            }]
        },
        //hpbooster: 3,
    }

    var homePlanets = {
        "Terran": "Earth",
        "Drengin": "Mars",
        "Arcanine": "Jupiter",
        "Pirate": "None",
    }

    function Star(sid, sname, sx, sy, sr, stemperature) {
        this.id = sid;
        this.name = sname;
        this.x = sx;
        this.y = sy;
        this.r = sr;
        this.temperature = stemperature;
    }

    function Planet(pid, pname, pangle, pr, porbitRadius, ppopulation, phabitability, prace, porbitting_star, phomePlanet, pimage_src) {
        this.id = pid;
        this.name = pname;
        this.x = 0;
        this.y = 0;
        this.angle = pangle;
        this.r = pr;
        this.orbitRadius = porbitRadius;
        this.population = ppopulation;
        this.habitability = phabitability;
        this.race = prace;
        this.orbittingStar = porbitting_star;
        this.homePlanet = phomePlanet;

        this.src = "images/planets/" + pimage_src;

        //colony-type = 0,1,2 (each type of colony has different armour properties making it harder to destroy)
        //orbitSpeed

        // add the following
        //r from sun - r for planet radius - r for orbit
    }

    function Moon(mid, mname, mangle, mr, morbitRadius, mpopulation, mhabitability, mrace, morbitting_planet) {
        this.id = mid;
        this.name = mname;
        this.x = 0;
        this.y = 0;
        this.angle = mangle;
        this.r = mr;
        this.orbitRadius = morbitRadius;
        this.population = mpopulation;
        this.habitability = mhabitability;
        this.race = mrace;
        this.orbittingPlanet = morbitting_planet;

        //make relationship between planet -> moon (planet owns moon)
    }


    // init world - move to another file
    var star = new Star(0, "Sun", 0, 0, 500, 5000);
    world.stars.Sun = star;

    var planet = new Planet(0, "Earth", Math.PI, 150, 50, 80000000, 80, "Terran", world.stars.Sun.name, true, "earth.png");
    world.planets[planet.name] = planet;

    // var planet = new Planet(0, "Mars", Math.PI+Math.PI/2, 200, 60, 3500000, 80, "Drengin", world.stars.Sun.name, true, "mars.png");
    var planet = new Planet(0, "Mars", Math.PI+Math.PI/2, 200, 60, 3500000, 80, "Terran", world.stars.Sun.name, true, "mars.png");
    world.planets[planet.name] = planet;

    var moon = new Moon(0, "Moon", Math.PI, 80, 15, 0.5, 50, "Terran", world.planets.Earth.name);
    world.moons[moon.name] = moon;



    //set up stars, planets, and moons
    // for (var key in world.planets) {
    //     var planet = world.planets[key];
    //     var orbittingStar = world.stars[planet.orbittingStar];

    //     planet.x = orbittingStar.x + (planet.r * Math.cos(planet.angle));
    //     planet.y = orbittingStar.y + (planet.r * Math.sin(planet.angle));


    //     world.planets[key] = planet;
    //     // console.log(planet);
    // }  

    // for (var key in world.moons) {
    //     var moon = world.moons[key];
    //     var orbittingPlanet = world.planets[moon.orbittingPlanet];

    //     moon.x = orbittingPlanet.x + (moon.r * Math.cos(moon.angle));
    //     moon.y = orbittingPlanet.y + (moon.r * Math.sin(moon.angle));

    //     world.moons[key] = moon;
    //     // console.log(moon);
    // }  


    //the stars in the world which planets will be orbitting around
    // world.stars = {
    //     0: {
    //         x: 500,
    //         y: 450,
    //         radius: 5000, //size of the star
    //         gravity: 100, // how much pull does the star provide
    //         burn_radius: 500, // this is the radius which ships would burn after entering
    //         temperature: 5000, //burn-radius + temperature should determine how much burn players get after entering the burn-radius
    //     }
    // }

    // world.earth = {
    //     population: "3.5b", //3.5 billion
    //     habitability: "100", //100% - this determines how fast the population grows
    //     race: "terran", //which race owns the planet
    //     radius: 5000, //how big the radius of the planet is
    //     orbit_offset: 500, //radius+orbit-offset = orbit radius
    //     x: 450, // starting position of the planet
    //     y: 500,
    //     //or we can go with radius from orbitting star (with a starting angle)
    //     // 

    //     //additional info about planet goes here
    //     //defense:
    // }
    
    /* Ship class specs are defines below
     * each race will have a different design, but same ship base specs (ships can be upgraded on starship)
     */

     //quick define weapon type, shoot will be a separate function
    // var weapons = {
    //     missile: {
    //         kind: "missile",
    //         capacity: 100, //how much space does it take to install (equip) the ship with this weapon
    //         pps:  20, //power per shot
    //         lifetime: 30, //how long the projectile lasts for,
    //         angle: 0,
    //         speed: 10, // 10 units per update
    //     },

    // }

    // bullets info
    function Missile() {
        this.kind = "missile";
        this.owner = -1; // will become the client id later on

        this.requirements = {
            rank: 0,
            credit: 0,
            capacity: 100,
        }

        this.specs = {
            width: 7,
            height: 7,
            damage: 15,
            pps: 20, // power required per shot
            //img: "some-image-source.png",
            // max_duration: 250, //how long does the projectile last for
        }

        this.pos = {
            x: 0,
            y: 0,
        }

        this.velocity = {
            speed: 5,
            angle: 0,
            x: 0,
            y: 0,
        }

        // this.duration = 0;

        // current ++ decay < max
        this.lifetime = {
            decay: 1, // how fast does the particle decay per update
            current: 0, // current life time (represents how much life time is left for the particle)
            max: 250, // max life time for the particle
        }

        // this.params = {

        // }
    }

    // bullets info
    function Rapid() {
        this.kind = "Rapid";
        this.owner = -1; // will become the client id later on

        this.requirements = {
            rank: 0,
            credit: 0,
            capacity: 100,
        }

        this.specs = {
            width: 5,
            height: 5,
            damage: 5,
            pps: 20, // power required per shot
            //img: "some-image-source.png",
            // max_duration: 250, //how long does the projectile last for
        }

        this.pos = {
            x: 0,
            y: 0,
        }

        this.velocity = {
            speed: 15,
            angle: 0,
            x: 0,
            y: 0,
        }

        // this.duration = 0;

        // current ++ decay < max
        this.lifetime = {
            decay: 1, // how fast does the particle decay per update
            current: 0, // current life time (represents how much life time is left for the particle)
            max: 200, // max life time for the particle
        }

        // this.params = {

        // }
    }

    // //weapons list
    // var weapons = {
    //     missile: new Missile(),
    //     rapid: new Rapid(),
    // }

    // var equipment = {
    //     shields: {
    //     },
    //     powers: {
    //     },
    //     engines: {
    //     },
    //     armors: {
    //     //hpbooster: 3,
    // }

    // basic ship layout
    function BasicShip() {
        this.destination = "None";

        this.pos = {
            x: 0, // location in x plane
            y: 0, // location in y plane
        }
        
        this.velocity = {
            x: 0,   // velocity in horizontal plane (-100:0:100) 
            y: 0,   // velocity in vertical y plane (-100:0:100)
            angle: Math.PI/2,   // current angle in radians of ship
            speed: 0,   // current speed speed 
            isAccelerating: 0,    // is the player accelerating   (-1:0:1) -> backwards: none : forward
            isRotating:0,    // is the player rotating (-1:0:1) -> left: none : right
        }
    }

    //small, fast, light
    function ScoutShip() {
        this.kind = "Scout";

        this.physx = new BasicShip();

        //requirements for unlocking the ship
        this.requirements = {
            rank: 0, 
            credit: 0,
        }

        //these are ship defaults that shouldn't not be changed
        this.specs = {
            weight: 140, // how much does the ship weight: to be used with engine and thursters to determine acceleration and max speed, ...
            capacity: 1000, // how much stuff can the ship hold
            max_crew: 50,  // crew members are required for the successful operation of the ship, and in order to not get docked (boarded)
            hp: 450, // how much health the ship has
            width: 20,
            height: 27,
            img: "images/assets/scout.png",
        }
        
        //these are the things you can upgrade during the game
        this.setup = {
            shields: { // how much damage can the ship take before affecting hp
                level: 0,
                current: 100, 
            },
            powers: {   // amount of energy the ship can has, certain things drain energy
                level: 0,
                current: 100,
            },
            engines: { // how much power the engine produces before it runs out of power
                level: 0,
                current: 100, 
            },

            // thrusters: 0, // side thursters that allow the ship to move sideways - strafe
            //cloak, armour
        }

        this.selectedWeapon = 0;
        this.weapons = {
            0: new Missile(),
            1: new Rapid(),
        }
    }

    function Starbase() {
        this.kind = "Starbase";

        this.physx = new BasicShip();

        //requirements for unlocking the ship
        this.requirements = {
            rank: 0, 
            credit: 0,
        }

        //these are ship defaults that shouldn't not be changed
        this.specs = {
            weight: 1200, // how much does the ship weight: to be used with engine and thursters to determine acceleration and max speed, ...
            capacity: 12000, // how much stuff can the ship hold
            max_crew: 6000,  // crew members are required for the successful operation of the ship, and in order to not get docked (boarded)
            hp: 1200, // how much health the ship has
            width: 150,
            height: 150,
            img: "images/assets/ships/terran-starbase.png",
        }
        
        //these are the things you can upgrade during the game
        this.setup = {
            shields: { // how much damage can the ship take before affecting hp
                level: 7,
                current: 100, 
            },
            powers: {   // amount of energy the ship can has, certain things drain energy
                level: 6,
                current: 100,
            },
            engines: { // how much power the engine produces before it runs out of power
                level: 4,
                current: 100, 
            },

            // thrusters: 0, // side thursters that allow the ship to move sideways - strafe
            //cloak, armour
        }

        this.selectedWeapon = 0;
        this.weapons = {
            0: new Missile(),
            1: new Missile(),
            2: new Rapid(),
            3: new Rapid(),
            4: new Rapid(),
            5: new Rapid(),
        }
    }

    //heavy, slow, big, strong
    function MahaullerShip() {
        // this.kind = "Mahauller",

    }


    /* Players and AI setups*/
    function Player(clientId) {
        this.client_id = clientId;
        this.race = "";

        this.details = {
            username: 'Anonymous',  // player username
            title: 'the spectator', // player rank
        }

        this.status = {
            status: "waiting",
            action: "spawning",
            state: "none", // action for docking, orbitting, and 
        }
    }

    function AI() {
        this.race = "";

        this.details = {
            username: 'Anonymous',  // player username
            title: 'the spectator', // player rank
        }

        this.status = {
            status: "waiting",
            action: "spawning",
            state: "none", // action for docking, orbitting, and 
        }
    }


    /*
     * Socket to use to broadcast monitoring events
     */
    var monitor = io.of('/monitor');
    monitor.on('connection', function(socket){
        socket.emit('update', {nbUsers: users, nbGames: Object.keys(games).length});
    });

    /*
     * Socket IO event handlers
     */
    io.sockets.on('connection', function (socket) {

        var username = socket.handshake.query.user;

        users++;
        client_offset++;
        // monitor.emit('update', {nbUsers: users, nbGames: Object.keys(games).length});

        /*
         * A player joins a game
         */
         //return a spectate mode until user joins/creates an account/etc...
        socket.on('join', function (data) {
            //
            //TODO : make this after the game can be returned as data, then player can spectate but not play

            // /* TODO: handle full case, a third player attempts to join the game after already 2 players has joined the game
            // if (game.status === "ready") {
            //     socket.emit('full');
            // io.sockets.to(room).emit('ready', { white: getPlayerName(room, "white"), black: getPlayerName(room, "black") });

            console.log('player has joined');
            var player = new Player(client_offset);
                
            player.details.username = 'Anonymous ' + player.client_id;
            player.details.title = 'The Conscript';

            socket.client_id = player.client_id;
            userlist[socket.client_id] = player;


            //wait for player to join game/create account and stuff
            socket.emit('wait', player);
            return;
        });

        //create player accoriding to data, and emit ready to tell player server is ready 
        socket.on('ready', function(playerData) {

            // fetch player
            var player = userlist[socket.client_id];

            // create player ship depending on their choices
            console.log("Ship: " + playerData.ship);
            var ship = new ScoutShip();

            console.log("New player id: " + player.client_id);
            console.log("Player info: " + playerData);

            // update player info
            // player = playerData;
            player.status.status = "ready"; // used to tell the server that the player has joined the game
            player.status.action = "spawning"; // action for docking, orbitting, and 
            
            player.details.username = playerData.username;
            player.race = playerData.race;

            // check player race to determine where they should spawn
            var startX, startY;
            var homePlanet = world.planets[homePlanets[player.race]];
            
            if (homePlanet == undefined) {
                homePlanet = world.planets["Earth"];
            } 

            ship.physx.pos.x = homePlanet.x;
            ship.physx.pos.y = homePlanet.y;
            ship.physx.destination = homePlanet.name;

            // if (player.race == "Terran") {

            // } else if (player.race == "Drengin") {

            // } else if (player.race == "Arcanine") {

            // } else if (player.race == "Pirate") {

            // }

            // console.log(player.race + " : " + player.ship);


            // add player attributes according to info
                // start location at home planet -> determined by race
                // ship player has chosen
            
            ship.physx.velocity = {
                x: 0,   // velocity in horizontal plane (-100:0:100) 
                y: 0,   // velocity in vertical y plane (-100:0:100)
                angle: Math.PI/2,   // current angle in radians of ship
                speed: 0,   // current speed speed 
                isAccelerating: 0,    // is the player accelerating   (-1:0:1) -> backwards: none : forward
                isRotating:0,    // is the player rotating (-1:0:1) -> left: none : right
            }

            // save player
            userlist[socket.client_id] = player;
            //get ship info
            //check if player ship is a starbase (if so then append it to starbases)
            ships[socket.client_id] = ship;

            // player.weapon = ship.weapon[0]; // currently selected weapon - default is ships primary weapon
            player.shots = []; // keep track of players fired weapons

            //get home planet and other world info

            player.status.action = "orbitting";
            socket.emit('ready', player, ship, "world-data");
        });

        socket.on("request-colony", function() {
            var player = userlist[socket.client_id];
            var playerShip = ships[player.client_id];
            var requestPlanet = world.planets[playerShip.physx.destination]; // maybe add destintion type unless moons and planets get merged and differentiated by kind or type

            if (player.race == requestPlanet.race) {
                console.log("requesting colony from: " + requestPlanet.name);
                //check if planet has colonies available
                //on accepting colony request, player should be given a portion of size

                var colonySize = 3000000;
                requestPlanet.population -= colonySize;

                //check if player already has attachment, check colony and increase if colony is available
                player.attachment = {
                    type: "colony",
                    size: colonySize,
                }

                userlist[socket.client_id] = player;
            }
        });

        socket.on("drop-attachment", function() {
            var player = userlist[socket.client_id];
            var playerShip = ships[player.client_id];

            if (player.attachment.type == "colony") {
                // add colony to free colony list
                var newColony = new Object();
                // newColony = player.attachment;
                
                console.log("Player speed: " + playerShip.physx.velocity.speed);
                newColony.velocity = {
                    x: playerShip.physx.velocity.x,
                    y: playerShip.physx.velocity.y,
                    angle: playerShip.physx.velocity.angle,
                    speed: playerShip.physx.velocity.speed,
                }

                var offsetX = 25*Math.cos(playerShip.physx.velocity.angle);
                var offsetY = 25*Math.sin(playerShip.physx.velocity.angle);

                newColony.pos = {
                    x: playerShip.physx.pos.x + offsetX,
                    y: playerShip.physx.pos.y + offsetY,
                }

                newColony.specs = {
                    size: player.attachment.size,
                    type: player.attachment.type,
                    race: player.race,
                }

                world.colonies.push(newColony);
            }
            delete player.attachment;
            userlist[socket.client_id] = player;
        });

        //changes the weapon equiped on the ship for firing
        socket.on("change-selected-weapon", function(selectedIndex) {
            var player = userlist[socket.client_id];
            var playerShip = ships[player.client_id];

            if (playerShip.weapons[selectedIndex] != undefined) {
                console.log("weapon changed");
                playerShip.selectedWeapon = selectedIndex;
            } else {
                console.log("weapon slot not available");
            }
        });


        //Shopping section (shopping only works while player is docked)
        socket.on("request-upgrade-equipment", function() {
            var player = userlist[socket.client_id];
            player.status.state = "in-dock";
            socket.emit("show-upgrade-equipment", equipment);
        });

        socket.on("done-upgrading", function() {
            var player = userlist[socket.client_id];
            player.status.state = "done-upgrading";
        });

        socket.on("request-upgrade", function(type, level) {
            var player = userlist[socket.client_id];
            var playerShip = ships[player.client_id];

            //check if player has enuff credits, rank, capacity, 
            playerShip.setup[type].level = level;
        });


        // player shot (add cooldown for weapons)
        socket.on("player-shoot-update", function() {
            var player = userlist[socket.client_id];
            var playerShip = ships[player.client_id];

            // player.weapon - check player weapon and fire accordingly
            var newBullet = new Object();
            if (playerShip.weapons[playerShip.selectedWeapon].kind == "missile") {
                newBullet = new Missile();
            } else if (playerShip.weapons[playerShip.selectedWeapon].kind == "Rapid") {
                newBullet = new Rapid();    
            }
            
            console.log("shooting: " + playerShip.weapons[playerShip.selectedWeapon].kind);

            // player pos + ship width/2 - height/2 * Math.cos(angle) to get front of ship
            newBullet.pos.x = playerShip.physx.pos.x;
            newBullet.pos.y = playerShip.physx.pos.y;

            newBullet.velocity.angle = (playerShip.physx.velocity.angle + Math.PI)%(Math.PI*2);

            // update bullet
            newBullet.velocity.x = newBullet.velocity.speed * (Math.cos(newBullet.velocity.angle));
            newBullet.velocity.y = newBullet.velocity.speed * (Math.sin(newBullet.velocity.angle));
            //add player's current speed to bullet speed if its a projectile

            player.shots.push(newBullet);
        });



        socket.on("start-accelerate-player-update", function() {
            ships[socket.client_id].physx.velocity.isAccelerating = 1;
        });

        socket.on("start-deccelerate-player-update", function() {
            ships[socket.client_id].physx.velocity.isAccelerating = -1;
        });

        socket.on("stop-accelerate-player-update", function() {
            // if (userlist[socket.client_id].velocity.accelerating == 1) {
                ships[socket.client_id].physx.velocity.isAccelerating = 0;
            // }
        });

        socket.on("stop-deccelerate-player-update", function() {
            // if (userlist[socket.client_id].velocity.accelerating == -1) {
                ships[socket.client_id].physx.velocity.isAccelerating = 0;
            // }
        });


        

        socket.on("start-rotate-player-left-update", function() {
            ships[socket.client_id].physx.velocity.isRotating = -1;
        });      

        socket.on("start-rotate-player-right-update", function() {
            ships[socket.client_id].physx.velocity.isRotating = 1;
        });

        socket.on("stop-rotate-player-left-update", function() {
            ships[socket.client_id].physx.velocity.isRotating = 0;
        });      

        socket.on("stop-rotate-player-right-update", function() {
            ships[socket.client_id].physx.velocity.isRotating = 0;
        });

        socket.on("go-to-orbit", function(selectedPlanet) {
            var player = userlist[socket.client_id];
            var playerShip = ships[socket.client_id];
            // console.log(selectedPlanet + ": " + world.planets[selectedPlanet].name);

            playerShip.physx.destination = selectedPlanet;
            player.status.action = "orbitting";  
            playerShip.physx.velocity.isAccelerating = 0;
        });

        socket.on("go-to-closest-orbit", function() {
            var player = userlist[socket.client_id];
            var playerShip = ships[player.client_id];

            var d = -1;
            // calculate closest planet or moon
            for (var key in world.planets) {
                var planet = world.planets[key];

                if (planet.race == player.race) {
                    var distance = Math.sqrt(Math.pow(planet.x - playerShip.physx.pos.x, 2) + Math.pow(planet.y - playerShip.physx.pos.y, 2));

                    if (d == -1 || distance < d) {
                        d = distance;
                        playerShip.physx.destination = key;
                    }
                }
            }

            if (d != -1) {
                player.status.action = "orbitting";    
                playerShip.physx.velocity.isAccelerating = 0;
            }
        });

        socket.on("go-to-dock", function() {
            var player = userlist[socket.client_id];
            var playerShip = ships[player.client_id];

            // find closest dock
            player.status.action = "docking";
            player.status.state = "docking";
            playerShip.physx.velocity.isAccelerating = 0;
        });

        /*
         * A player makes a new move => broadcast that move to the opponent
         */
        socket.on('new-move', function(data) {
            socket.broadcast.to(data.token).emit('new-move', data);
        });

        /*
         * A player resigns => notify opponent, leave game room and delete the game
         */
        // socket.on('resign', function (data) {
        //     var room = data.token;
        //     if (room in games) {
        //         io.sockets.to(room).emit('player-resigned', {
        //             'side': data.side
        //         });
        //         games[room].players[0].socket.leave(room);
        //         games[room].players[1].socket.leave(room);
        //         delete games[room];
                // monitor.emit('update', {nbUsers: users, nbGames: Object.keys(games).length});
            // }


        //});

        socket.on('request-orbit-planets', function() {
            socket.emit('display-orbit-planets', world.planets);
        });

        /*
         * A player disconnects => notify opponent, leave game room and delete the game
         */
        socket.on('disconnect', function(data){
            users--;
            delete userlist[socket.client_id];
            // monitor.emit('update', {nbUsers: users, nbGames: Object.keys(games).length});
            // for (var token in games) {
            //     var game = games[token];
            //     for (var p in game.players) {
            //         var player = game.players[p];
            //         if (player.socket === socket) {
            //             socket.broadcast.to(token).emit('opponent-disconnected');
            //             delete games[token];
            //             monitor.emit('update', {nbUsers: users, nbGames: Object.keys(games).length});
            //         }
            //     }
            // }


        });
    });

// take in player angle, and destination angle
// return angle to be assigned to the player ship
function fixateShipAngle(targetPlayer, destinationAngle) {

    // do not override player input
    if (targetPlayer.velocity.isRotating == 0) {
        var playerAngle = targetPlayer.velocity.angle%(2*Math.PI);
        destinationAngle = destinationAngle%(2*Math.PI);

        
        
        // if (targetPlayer.velocity.angle >= destinationAngle + 1/Math.PI && targetPlayer.velocity.angle <= destinationAngle - 1/Math.PI) {
        // // check current angle and destination and determine the shortest way to turn
        //     if ((destinationAngle - targetPlayer.velocity.angle) <= Math.PI) { // less than half pie, close the angle
        //         console.log("turning right");
        //         targetPlayer.velocity.angle -= 1/Math.PI;
        //     } else {
        //         console.log("turning left");
        //         targetPlayer.velocity.angle += 1/Math.PI;
        //     }
        // } else {
        //     console.log("corrent angle");
        //     targetPlayer.velocity.angle = destinationAngle;
        // }
        // check current angle and ship turn rate to determine if the ship should turn


        // if the distance is very small, then just put the ship on course
    } 
}

function updatePlayers() {
    // every frame update on server side happens here
    for (var key in userlist) {
        player = userlist[key];
        if (player.status.status == "ready") {

            var playerShip = ships[player.client_id];
            //PhysX

            // implement ship turn rate for more info about this
            // free style turning
            console.log(player.client_id +":"+ playerShip.physx.velocity.isRotating);
            console.log(player.client_id +":"+ playerShip.physx.velocity.isAccelerating);
            switch (playerShip.physx.velocity.isRotating) {
                case -1: //left
                    playerShip.physx.velocity.angle -= 0.1 //ship turn rate should be taken into account later
                    // player.velocity.x = player.velocity.x - 1;
                break;
                case 0: // none

                break;
                case 1: // right
                    playerShip.physx.velocity.angle += 0.1 //ship turn rate should be taken into account later
                    // player.velocity.x = player.velocity.x + 1;
                break;
            }

            // take into account weight, engine power, etc...
            switch(playerShip.physx.velocity.isAccelerating) {
                case -1: // accelerating
                 // use angle to know which direction the player is moving
                    player.status.action = "roaming";
                    player.status.state = "none";
                    
                    var angle = playerShip.physx.velocity.angle%(2*Math.PI);

                    xvel = Math.cos(angle);
                    yvel = Math.sin(angle);

                    playerShip.physx.velocity.x += xvel;
                    playerShip.physx.velocity.y += yvel;
                break;
                case 0: // none
                    //reduce speed due to air resistance
                    if (player.status.action == "orbitting") {
                        var orbitDest = world.planets[playerShip.physx.destination];
                        x = orbitDest.x;
                        y = orbitDest.y;
                        r = orbitDest.orbitRadius;

                        // x = 400;
                        // y = 250;    
                        // r = 200; //radius
                       
                        // quadrant = "N";  //neutral

                        // if (player.pos.x - x > 0) {
                        //     if (player.pos.y - y > 0) {
                        //         quadrant = "A";
                        //     } else {
                        //         quadrant = "C";
                        //     }
                        // } else {
                        //     if (player.pos.y - y > 0) {
                        //         quadrant = "S";
                        //     } else {
                        //         quadrant = "T";
                        //     }
                        // }
                        // console.log("quadrant: " + quadrant);
                        // console.log("player angle: " + player.velocity.angle%(2*Math.PI));

                        //check distance to planet and start orbitting
                        var destAngle = Math.atan2(y - playerShip.physx.pos.y, x - playerShip.physx.pos.x)%(2*Math.PI); //y / x


                        //distance
                        var d = Math.sqrt(Math.pow(x - playerShip.physx.pos.x,2) + Math.pow(y - playerShip.physx.pos.y, 2));

                        if (d < r - 30) {
                            // console.log("inside-orbit");
                            
                            xvel = Math.cos(destAngle);
                            yvel = Math.sin(destAngle);

                            playerShip.physx.velocity.x = -xvel;
                            playerShip.physx.velocity.y = -yvel;


                            

                            if (playerShip.physx.velocity.isRotating == 0) {
                                // fixateShipAngle(player, destAngle%(Math.PI*2));
                                playerShip.physx.velocity.angle = destAngle%(Math.PI*2);
                            }                                
                        } else if (d <= r + 30) {
                            // console.log("in-orbit");
                            player.status.state = "in-orbit";

                            
                            if (playerShip.physx.velocity.isRotating == 0) {
                                // fixateShipAngle(player, (destAngle - Math.PI/2)%(Math.PI*2));
                                playerShip.physx.velocity.angle = destAngle - Math.PI/2 %(Math.PI*2);
                            }

                            //increase angle
                            destAngle -= Math.PI/180;

                            destX = x+(r*Math.cos(destAngle + Math.PI));
                            destY = y+(r*Math.sin(destAngle + Math.PI));

                            orbitAngle = Math.atan2(destY - playerShip.physx.pos.y, destX - playerShip.physx.pos.x);

                            xvel = Math.cos(orbitAngle);
                            yvel = Math.sin(orbitAngle);

                            playerShip.physx.velocity.x = xvel;
                            playerShip.physx.velocity.y = yvel;
                        } else {
                            // console.log("finding-orbit");
                            
                            xvel = Math.cos(destAngle);
                            yvel = Math.sin(destAngle);

                            if (d < r+100) {
                                playerShip.physx.velocity.x = xvel;
                                playerShip.physx.velocity.y = yvel;
                            } else {
                                playerShip.physx.velocity.x = xvel*50;
                                playerShip.physx.velocity.y = yvel*50;
                            }     


                            
                            if (playerShip.physx.velocity.isRotating == 0) {
                                // fixateShipAngle(player, (destAngle + Math.PI)%(Math.PI*2));
                                playerShip.physx.velocity.angle = destAngle + Math.PI%(Math.PI*2);
                            }
                            
                        }
                    } else if (player.status.action == "docking") {
                        x = 200;
                        y = 250;    
                        r = 50; //radius

                        //check distance to planet and start orbitting
                        var destAngle = Math.atan2(y - playerShip.physx.pos.y, x - playerShip.physx.pos.x)%(2*Math.PI); //y / x


                        //distance
                        var d = Math.sqrt(Math.pow(x - playerShip.physx.pos.x,2) + Math.pow(y - playerShip.physx.pos.y, 2));

                        if (d < r - 5) {
                            // console.log("inside-orbit");
                            player.status,state == "docking"

                            xvel = Math.cos(destAngle);
                            yvel = Math.sin(destAngle);

                            playerShip.physx.velocity.x = -xvel;
                            playerShip.physx.velocity.y = -yvel;


                            

                            if (playerShip.physx.velocity.isRotating == 0) {
                                // fixateShipAngle(player, destAngle%(Math.PI*2));
                                playerShip.physx.velocity.angle = destAngle%(Math.PI*2);
                            }                                
                        } else if (d <= r + 5) {
                            // console.log("in-orbit");
                            if (player.status.state == "docking") {
                                player.status.state = "docked";
                            } else if (player.status.state != "done-upgrading" && player.status.state != "in-dock") {
                                player.status.state = "in-dock";
                            }

                            //refresh player equipment

                            
                            if (playerShip.physx.velocity.isRotating == 0) {
                                // fixateShipAngle(player, (destAngle - Math.PI/2)%(Math.PI*2));
                                playerShip.physx.velocity.angle = destAngle - Math.PI/2 %(Math.PI*2);
                            }

                            //increase angle
                            destAngle -= Math.PI/180;

                            destX = x+(r*Math.cos(destAngle + Math.PI));
                            destY = y+(r*Math.sin(destAngle + Math.PI));

                            orbitAngle = Math.atan2(destY - playerShip.physx.pos.y, destX - playerShip.physx.pos.x);

                            xvel = Math.cos(orbitAngle);
                            yvel = Math.sin(orbitAngle);

                            playerShip.physx.velocity.x = xvel;
                            playerShip.physx.velocity.y = yvel;
                        } else {
                            // console.log("finding-orbit");
                            player.status.state == "docking"
                            
                            xvel = Math.cos(destAngle);
                            yvel = Math.sin(destAngle);

                            if (d < r+100) {
                                playerShip.physx.velocity.x = xvel;
                                playerShip.physx.velocity.y = yvel;
                            } else {
                                playerShip.physx.velocity.x = xvel*50;
                                playerShip.physx.velocity.y = yvel*50;
                            }     


                            
                            if (playerShip.physx.velocity.isRotating == 0) {
                                // fixateShipAngle(player, (destAngle + Math.PI)%(Math.PI*2));
                                playerShip.physx.velocity.angle = destAngle + Math.PI%(Math.PI*2);
                            }
                            
                        }
                    }
                break;
                case 1: // deccelerating
                    player.status.action = "roaming";
                    player.status.state = "none";

                    var angle = playerShip.physx.velocity.angle%(2*Math.PI);

                    xvel = Math.cos(angle);
                    yvel = Math.sin(angle);

                    // yvel = yvel/(xvel+yvel);
                    // xvel = xvel/(xvel+yvel);

                    playerShip.physx.velocity.x -= xvel;
                    playerShip.physx.velocity.y -= yvel;
                break;
            }

            playerShip.physx.pos.x = playerShip.physx.pos.x + playerShip.physx.velocity.x;
            playerShip.physx.pos.y = playerShip.physx.pos.y + playerShip.physx.velocity.y;



            //process player bullets
            for (var i = 0; i < player.shots.length; i++) {
                // get bullet
                var bullet = player.shots[i];

                // current + decay <= max
                bullet.lifetime.current += bullet.lifetime.decay;

                //check if bullet has expired
                if (bullet.lifetime.current >= bullet.lifetime.max) {
                    player.shots.splice(i, 1);
                } else {

                    // save new bullet position
                    bullet.pos.x = bullet.pos.x + bullet.velocity.x;
                    bullet.pos.y = bullet.pos.y + bullet.velocity.y;

                    //check for collision
                    //check ships, planets, and moons
                    for (var enemyKey in userlist) {
                        var enemyPlayer = userlist[enemyKey];
                        var enemyShip = ships[enemyPlayer.client_id]; 

                        // check race rather than client_id
                        if (enemyPlayer.status.status == "ready" && player.race != enemyPlayer.race) {
                            if ((bullet.pos.x >= enemyShip.physx.pos.x - enemyShip.specs.width/2 && bullet.pos.x <= enemyShip.physx.pos.x + enemyShip.specs.width/2) && 
                                (bullet.pos.y >= enemyShip.physx.pos.y - enemyShip.specs.height/2 && bullet.pos.y <= enemyShip.physx.pos.y + enemyShip.specs.height/2)) {
                                    console.log("collision detected: " + player.client_id + " hits " + enemyPlayer.client_id);

                                    enemyShip.specs.hp -= bullet.specs.damage; // update ship power && other stuff
                                    player.shots.splice(i, 1);
                                }
                        }
                    }
                }
            }
        }
    }
}

function updateWeapons() {

}

function collisionDetection() {

}

function updateStars() {

}

function updatePlanets() {
    for (var key in world.planets) {
        var planet = world.planets[key];
        var orbittingStar = world.stars[planet.orbittingStar];

        planet.angle += 0.0001; // += orbit-speed
        planet.x = orbittingStar.x + (planet.r * Math.cos(planet.angle));
        planet.y = orbittingStar.y + (planet.r * Math.sin(planet.angle));


        world.planets[key] = planet;
        // console.log(planet);
    }  
}

function updateMoons() {
    for (var key in world.moons) {
        var moon = world.moons[key];
        var orbittingPlanet = world.planets[moon.orbittingPlanet];

        moon.angle += 0.0005; // += orbit-speed
        moon.x = orbittingPlanet.x + (moon.r * Math.cos(moon.angle));
        moon.y = orbittingPlanet.y + (moon.r * Math.sin(moon.angle));

        world.moons[key] = moon;
        // console.log(moon);
    }  
}

function updateColonies() {
    // check for pick ups

    // update dropped colonies to deploy on planets when in range
    for (var key in world.colonies) {
        var colony = world.colonies[key];

        if (colony.velocity.x >= 1) {
            colony.velocity.x -= 0.01;
        } else if (colony.velocity.x <= -1) {
            colony.velocity.x += 0.01;
        } 

        if (colony.velocity.y >= 1) {
            colony.velocity.y -= 0.01;
        } else if (colony.velocity.y <= -1) {
            colony.velocity.y += 0.01;
        }


        xvel = colony.velocity.x * Math.cos(colony.velocity.angle);
        yvel = colony.velocity.y * Math.sin(colony.velocity.angle);

        colony.pos.x += colony.velocity.x;
        colony.pos.y += colony.velocity.y;

        // do the same for moons
        for (var planetKey in world.planets) {
            var planet = world.planets[planetKey];

            // calculate distance between planet and colony          
            var distance = Math.sqrt(Math.pow(planet.x - colony.pos.x, 2) + Math.pow(planet.y - colony.pos.y, 2));

            // if planet is in range, then deploy colony, get rid of colony, recalculate planet population
            if (distance < planet.orbitRadius) { 
                console.log("deploying colony on planet: " + planet.name + ": " + planet.population);

                if (planet.race == colony.race) {
                    planet.population += colony.specs.size;
                    console.log("Adding to friendly planet: " + planet.population);

                    world.colonies.splice(key, 1);
                } else {
                    planet.population -= colony.specs.size;
                    if (planet.population < 0) {
                        planet.race = colony.specs.race;
                        planet.population = -(planet.population);
                    }

                    console.log("Conquering enemy planet: " + planet.population);

                    world.colonies.splice(key, 1);
                }
            }
            

            // conquer planet if possible
        }
    }  
}

function updateGame() {
    // world
    updateStars();
    updatePlanets(); //update population and colony availability flag
    updateMoons();

    // players
    updatePlayers();
    updateWeapons();
    collisionDetection(); // go through all bullets and check if they hit any player

    updateColonies();
}


// update players on regular interval
// update planets on a lesser interval
setInterval(function () {
    updateGame();
    io.emit('update', userlist, ships, world);
}, 42);

    /*
     * Utility function to find the player name of a given side.
     */
    function getPlayerName(room, side) {
        var game = games[room];
        for (var p in game.players) {
            var player = game.players[p];
            if (player.side === side) {
                return player.name;
            }
        }
    }
};